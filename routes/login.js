const express = require('express');
const router = express.Router();
const jwt = require('../helpers/jwt');
const {exists, isEmail, checkBody, filter, parseBool} = require("../helpers/middleware");

router.post('', exists('password'),isEmail('email'),async (req,res)=>{
    try{
        checkBody(req);
        res.status(200).send(await jwt.sign({email:req.body.email, role: 'admin'}));
    }
    catch(err){
        res.status(err.code || 400).send(err)
    }
});

module.exports = router;