const express = require('express');
const router = express.Router();
const db = require('../collections');
const {exists, isAuth, guest, checkBody, filter, isLatLong, parseBool} = require('../helpers/middleware');

router.post('', filter(...db.Orders.fields), isLatLong('delivery', 'pickup'),parseBool("isPrivate") ,isAuth,async (req,res)=>{
    try{
        checkBody(req);
        const order = await db.Orders.create(req.body, req.user.email);
        res.status(201).send(order)
    }
    catch(err){
        res.status(err.code || 400).send(err)
    }
});

router.get('/:id', guest, async(req,res)=>{
    try{
        const order = await db.Orders.get(db.ObjectId(req.params.id), req.user.role);
        res.status(200).send(order)
    }
    catch(err){
        res.status(err.code || 400).send(err)
    }
});

router.get('', guest, async (req,res)=>{
    try{
        const order = await db.Orders.getAll(req.user.role, req.query);
        res.status(200).send(order)
    }
    catch(err){
        res.status(err.code || 400).send(err)
    }
});

router.patch('/:id', guest, filter(...db.Orders.fields) , parseBool("isPrivate"),async(req,res)=>{
    try{
        const updatedOrder = await db.Orders.update(db.ObjectId(req.params.id), req.body);
        res.status(200).send(updatedOrder);
    }
    catch(err){
        res.status(err.code || 400).send(err)
    }
});

module.exports = router;