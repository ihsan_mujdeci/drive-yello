const express = require('express');
const app = express();
const bodyParser = require('body-parser');

function serverListen(app, port){
    return new Promise((resolve, reject)=>{
        app.listen(port, ()=>{
            console.log(`App listening on port :${port}`);
            resolve(app)
        })
    })
}

function routes(router){
    return [
        { url : '/orders',   route : './routes/orders' },
        { url : '/login',    route : './routes/login' },
    ]
    .reduce((router,route) => router.use(route.url, require(route.route)), router);
}

async function init(port){
    if(module.exports.app)
        return module.exports.app;

    app.use(bodyParser.json({ limit: '5mb' }));
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(function(req, res, next) {
        req.checkBody = []; //Used for checking body middleware
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Accept, Authorization");
        res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS, PATCH");
        next();
    });

    app.use(routes(express.Router()));
    await serverListen(app, port);
    module.exports.app = app;

    return app
}

module.exports= {
    init
};