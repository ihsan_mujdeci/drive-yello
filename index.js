require('./config/config');
const db = require('./collections');
const app = require('./app');

void async function bootstrap(){
    try{
        await db.init(process.env.db_port, process.env.db_name);
        await app.init(process.env.server_port);
    }
    catch(err){
        console.log('Error in bootstrap',err)
    }
}();