
class Base{
    constructor(Mongo, name){
        this.collection = Mongo.collection(name);
        this.name = name;
    }

    find(query, projection){
        return this.collection.find(query, projection).toArray()
    }

    async sureFindOneAndUpdate(query, update, options = {}, error=`Couldn't find ${this.name} to update`){
        options.returnOriginal = false;

        let u = await this.collection.findOneAndUpdate(query, update, options);

        if(!u.lastErrorObject.updatedExisting){
            throw {message: error}
        }

        return u.value;
    };

    async sureFindOne(query, project = {}, error = `${this.name} wasn't found`){
        const f = await this.collection.findOne(query, project);

        if(!f)
            throw {message: error};

        return f
    }

    async insertOne(document){
        document.createdAt = new Date().toISOString();
        const i = await this.collection.insertOne(document);
        return i.ops[0]
    }
}

module.exports = Base;