const Base = require('./Base');
const {googleDirections} = require('../helpers/directions');

class Orders extends Base{
    constructor(Mongo, name){
        super(Mongo, name);

        this.fields =[
            'pickup',
            'delivery',
            'assignedTo',
            'isPrivate',
            'restaurant'
        ]
    }

    async create({pickup, delivery, assignedTo, isPrivate=true, restaurant, deliveryAt}, createdBy){

        const directions = await googleDirections(pickup, delivery);

        return this.insertOne({
            pickup,
            delivery,
            assignedTo,
            isPrivate,
            restaurant,
            createdBy,
            distance: directions.distance,
            duration: directions.duration,
            pickupAddress: directions.pickupAddress,
            deliveryAddress: directions.deliveryAddress
        })
    }

    async update(orderId, fields){
        const foundOrder = await this.sureFindOne({_id: orderId});

        if(fields.delivery || fields.pickup){
            if( (foundOrder.delivery !== fields.delivery) || (foundOrder.pickup !== fields.pickup) )
            {
                const directions = await googleDirections(fields.pickup || foundOrder.pickup, fields.delivery || foundOrder.delivery);

                fields.distance = directions.distance;
                fields.duration = directions.duration;
                fields.pickupAddress = directions.pickupAddress;
                fields.deliveryAddress = directions.deliveryAddress;
            }
        }

        return this.sureFindOneAndUpdate({_id: orderId},{$set:fields});
    }

    async get(orderId, role){
        const query = {_id: orderId};

        if(role === "guest"){
            query.isPrivate = false
        }

        return this.sureFindOne(query)
    }

    async getAll(role, {createdAt=-1, page=1, items=50}){
        const query = {};

        if(role === "guest"){
            query.isPrivate = false
        }

        return this.collection.find(query)
            .skip((parseInt(page)-1)*parseInt(items))
            .limit(parseInt(items))
            .sort({createdAt:parseInt(createdAt)})
            .toArray()
    }

}

module.exports = Orders;