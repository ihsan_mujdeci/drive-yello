const mongodb = require('mongodb');
const Orders = require('./Orders');

async function init(port, name){
    if (module.exports.Mongo)
        return module.exports.Mongo;

    const Mongo = await mongodb.connect(`mongodb://localhost:${port}/${name}`);
    module.exports.Orders = new Orders(Mongo, 'Orders');
    module.exports.Mongo = Mongo;

    console.log(`Mongo ${name} running on :${port}`);

    return Mongo
}

module.exports = {
    init : init,
    ObjectId: mongodb.ObjectId
};
