const db = require('../../collections');
const app = require('../../app');
const chai = require('chai');
const should = chai.should();
let chaiHttp = require("chai-http");
const jwt = require('../../helpers/jwt');
chai.use(chaiHttp);

describe("Login routes",()=>{

    let server;

    before(async ()=>{
       await db.init(process.env.db_port, process.env.db_name);
       server = await app.init(process.env.server_port)
    });

    it("Should not Work, no body is set",(done)=>{
        chai.request(server)
            .post('/login')
            .end((err, res) => {
                res.body.should.deep.equal(
                    [
                        { param: 'password', message: 'password is not included in request' },
                        { param: 'email', message: 'email is not valid email format' }
                    ]
                )

                done()
            });
    });

    it("Incorrect email",(done)=>{
        chai.request(server)
            .post('/login')
            .send({email: "Incorrect email", password: "password"})
            .end((err, res) => {
                res.body.should.deep.equal(
                    [
                        { param: 'email', message: 'email is not valid email format' }
                    ]
                );
                done()
            });
    });

    it("Should Login Properly",(done)=>{
        chai.request(server)
            .post('/login')
            .send({email: "mail@test.com"})
            .end((err, res) => {
                res.body.should.deep.equal(
                    [
                        { param: 'password', message: 'password is not included in request' },
                    ]
                )
                done()
            });
    })

    it("Should Login Properly",(done)=>{
        chai.request(server)
            .post('/login')
            .send({email: "mail@test.com", password: 'password'})
            .end(async (err, res) => {
                try{
                    const token = await jwt.verify(res.text);
                    token.email.should.equal("mail@test.com");
                    done()
                }
                catch(err){
                    should.fail();
                    done()
                }
            });
    })

});