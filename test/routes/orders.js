const db = require('../../collections');
const app = require('../../app');
const chai = require('chai');
const should = chai.should();
let chaiHttp = require("chai-http");
chai.use(chaiHttp);
const jwt = require('../../helpers/jwt');

const takapuna = "-36.787304,174.770237";
const palmerstonNorth = "-40.354859,175.610485";
const wiri = "-37.003122,174.864392";
const christChurch = "-43.533890,172.651522";
const wellington = "-41.293421,174.794841";

const order= {
    delivery:takapuna,
    pickup: palmerstonNorth,
    restaurant: "Mom & Pop",
    assignedTo: 123,
};

const getOrders = [
    {
        delivery:takapuna,
        pickup: palmerstonNorth,
        restaurant: "Mom & Pop",
        assignedTo: db.ObjectId(),
        isPrivate: true
    },
    {
        delivery:takapuna,
        pickup: wiri,
        restaurant: "McDonalds",
        assignedTo: db.ObjectId(),
        isPrivate: true
    },
    {
        delivery:takapuna,
        pickup: christChurch,
        restaurant: "El Jannah",
        assignedTo: db.ObjectId(),
        isPrivate: true
    },
    {
        delivery:takapuna,
        pickup: palmerstonNorth,
        restaurant: "Albert's Muffins",
        assignedTo: db.ObjectId(),
        isPrivate: true
    },
    {
        delivery:takapuna,
        pickup: christChurch,
        restaurant: "Reyhana's",
        assignedTo: db.ObjectId(),
        isPrivate: true
    },
    {
        delivery:wiri,
        pickup: takapuna,
        restaurant: "Paradise",
        assignedTo: db.ObjectId(),
        isPrivate: false
    },
    {
        delivery:wiri,
        pickup: wellington,
        restaurant: "Starbucks",
        assignedTo: db.ObjectId(),
        isPrivate: false
    },
    {
        delivery:wiri,
        pickup: palmerstonNorth,
        restaurant: "Nino's pizza",
        assignedTo: db.ObjectId(),
        isPrivate: false
    }
];

// Pre made, google queries took too long
const createdGetOrders = [
    {   _id: '5a10e038faa02316d3f774d6',
        delivery: '-37.003122,174.864392',
        pickup: '-40.354859,175.610485',
        restaurant: 'Nino\'s pizza',
        assignedTo: '5a10e033faa02316d3f774cb',
        isPrivate: false,
        createdAt: '2017-11-19T01:36:56.984Z'
    },
    {
        _id: '5a10e038faa02316d3f774d5',
        delivery: '-37.003122,174.864392',
        pickup: '-41.293421,174.794841',
        restaurant: 'Starbucks',
        assignedTo: '5a10e033faa02316d3f774ca',
        isPrivate: false,
        createdAt: '2017-11-19T01:36:56.481Z'
    },
    {
        _id: '5a10e037faa02316d3f774d4',
        delivery: '-37.003122,174.864392',
        pickup: '-36.787304,174.770237',
        restaurant: 'Paradise',
        assignedTo: '5a10e033faa02316d3f774c9',
        isPrivate: false,
        createdAt: '2017-11-19T01:36:55.979Z'
    },
    {
        _id: '5a10e037faa02316d3f774d3',
        delivery: '-36.787304,174.770237',
        pickup: '-43.533890,172.651522',
        restaurant: 'Reyhana\'s',
        assignedTo: '5a10e033faa02316d3f774c8',
        isPrivate: true,
        createdAt: '2017-11-19T01:36:55.476Z'
    },
    {
        _id: '5a10e036faa02316d3f774d2',
        delivery: '-36.787304,174.770237',
        pickup: '-40.354859,175.610485',
        restaurant: 'Albert\'s Muffins',
        assignedTo: '5a10e033faa02316d3f774c7',
        isPrivate: true,
        createdAt: '2017-11-19T01:36:54.972Z'
    },
    {
        _id: '5a10e036faa02316d3f774d1',
        delivery: '-36.787304,174.770237',
        pickup: '-43.533890,172.651522',
        restaurant: 'El Jannah',
        assignedTo: '5a10e033faa02316d3f774c6',
        isPrivate: true,
        createdAt: '2017-11-19T01:36:54.469Z'
    },
    {
        _id: '5a10e035faa02316d3f774d0',
        delivery: '-36.787304,174.770237',
        pickup: '-37.003122,174.864392',
        restaurant: 'McDonalds',
        assignedTo: '5a10e033faa02316d3f774c5',
        isPrivate: true,
        createdAt: '2017-11-19T01:36:53.967Z'
    },
    {
        _id: '5a10e035faa02316d3f774cf',
        delivery: '-36.787304,174.770237',
        pickup: '-40.354859,175.610485',
        restaurant: 'Mom & Pop',
        assignedTo: '5a10e033faa02316d3f774c4',
        isPrivate: true,
        createdAt: '2017-11-19T01:36:53.465Z'
    }
]


const user ={
    email: "test@script.com",
    role: "admin"
};


const createdOrder = {
    pickup: '-40.354859,175.610485',
    pickupAddress: "173-175 The Square, Palmerston North, 4410, New Zealand",
    delivery: '-36.787304,174.770237',
    deliveryAddress: "296-298 Wiri Station Rd, Wiri, Auckland 2104, New Zealand",
    assignedTo: 123,
    isPrivate: true,
    restaurant: 'Mom & Pop',
    deliveryAt: undefined,
    createdBy: 'test@script.com',
    distance: '523 km',
    duration: '6 hours 29 mins',
    createdAt: '2017-11-18T04:57:40.440Z',
    _id: db.ObjectId("5a0fbdc43e21f9689884c431")
};


describe("Orders routes",()=>{

    let server;
    let token;

    before(async ()=>{
        await db.init(process.env.db_port, process.env.db_name);
        server = await app.init(process.env.server_port)
        token = await jwt.sign(user)
    });

    describe('POST /orders',()=>{
        it("Should fail make an order, no pickup and delivery",(done)=>{
            chai.request(server)
                .post('/orders')
                .set({authorization: "Basic "+token})
                .end((err, res) => {
                    err.response.body.should.deep.equal(
                        [
                            { param: 'delivery', message: 'Please include delivery' } ,
                            { param: 'pickup', message: 'Please include pickup' }
                        ]
                    );
                    done()
                });
        });

        it("Should fail make an order, no delivery",(done)=>{
            chai.request(server)
                .post('/orders')
                .set({authorization: "Basic "+token})
                .send({pickup: takapuna})
                .end((err, res) => {
                    err.response.body.should.deep.equal(
                        [ { param: 'delivery', message: 'Please include delivery' } ]
                    );
                    done()
                });
        })

        it("Should fail make an order, no pickup",(done)=>{
            chai.request(server)
                .post('/orders')
                .set({authorization: "Basic "+token})
                .send({delivery: takapuna})
                .end((err, res) => {
                    err.response.body.should.deep.equal(
                        [ { param: 'pickup', message: 'Please include pickup' } ]
                    );
                    done()
                });
        });

        it("Should fail make an order, no pickup only has long no lat",(done)=>{
            chai.request(server)
                .post('/orders')
                .set({authorization: "Basic "+token})
                .send({delivery: takapuna, pickup: "174.864392"})
                .end((err, res) => {
                    err.response.body.should.deep.equal(
                        [ { param: 'pickup', message: 'Only 2 items in lat long' } ]
                    );
                    done()
                });
        });

        it("Should fail make an order, pickup has malformed lat long",(done)=>{
            chai.request(server)
                .post('/orders')
                .set({authorization: "Basic "+token})
                .send({delivery: takapuna, pickup: "here,there"})
                .end((err, res) => {
                    err.response.body.should.deep.equal(
                        [ { param: 'pickup', message: "Lat long must be Int or float" } ]
                    );
                    done()
                });
        });

        it("Should successfully create an order",(done)=>{
            chai.request(server)
                .post('/orders')
                .set({authorization: "Basic "+token})
                .send(order)
                .end((err, res) => {

                    res.body.should.have.property('createdBy', user.email);
                    res.body.should.have.property('isPrivate', true);
                    res.body.should.have.property('restaurant', 'Mom & Pop');
                    res.body.should.have.property('duration');
                    res.body.should.have.property('distance');
                    res.body.should.have.property('pickupAddress');
                    res.body.should.have.property('deliveryAddress');
                    res.body.should.have.property('delivery', takapuna);
                    res.body.should.have.property('pickup', palmerstonNorth);

                    done()
                });
        })

        afterEach(()=>{
            return db.Orders.collection.remove({})
        })
    });

    describe('Get /orders/:id', ()=>{

        let privateListing, publicListing;

        before(async ()=>{
            const listings =  await Promise.all([
                db.Orders.create(order, user.email),
                db.Orders.create({...order, isPrivate: false}, user.email)
            ]);
            privateListing = listings[0];
            publicListing = listings[1]
        });

        it('Should get a private listing', (done)=>{
            chai.request(server)
                .get('/orders/'+privateListing._id.toString())
                .set({authorization: "Basic "+token})
                .send(order)
                .end((err, res) => {
                    privateListing._id.toString().should.equal(res.body._id);
                    done()
                });
        });

        it('Should get a public listing even though no auth token', (done)=>{
            chai.request(server)
                .get('/orders/'+publicListing._id.toString())
                .send(order)
                .end((err, res) => {
                    publicListing._id.toString().should.equal(res.body._id);
                    done()
                });
        });

        it('Should fail to get a listing because no auth token and listing is private', (done)=>{
            chai.request(server)
                .get('/orders/'+privateListing._id.toString())
                .send(order)
                .end((err, res) => {
                    err.response.body.message.should.equal("Orders wasn't found");
                    done()
                });
        });

        it('Should fail to get a listing because invalid listing Id', (done)=>{
            chai.request(server)
                .get('/orders/'+'5a0fa639345c1f47233f4f32')
                .set({authorization: "Basic "+token})
                .send(order)
                .end((err, res) => {
                    err.response.body.message.should.equal("Orders wasn't found");
                    done()
                });
        });

        after(()=>{
            return db.Orders.collection.remove({})
        })

    });

    describe('GET /listings, all listings with pagination and sorting',()=>{
        before(async ()=>{
            return db.Orders.collection.insert(createdGetOrders)
        });

        it('Should get all listings',done=>{
            chai.request(server)
                .get('/orders')
                .set({authorization: "Basic "+token})
                .send(order)
                .end((err, res) => {
                    res.body.should.have.length(getOrders.length);
                    done();
                });
        });


        it('Should get public only listings',done=>{
            chai.request(server)
                .get('/orders')
                .send(order)
                .end((err, res) => {
                    const privateListingsLength = 5;
                    const publicListingsLength = getOrders.length - privateListingsLength;
                    res.body.should.have.length(publicListingsLength);
                    done();
                });
        });

        it('Should get first 5 listings',(done)=>{
            chai.request(server)
                .get('/orders?items=5')
                .set({authorization: "Basic "+token})
                .send(order)
                .end((err, res) => {
                    res.body.should.have.length(5);
                    res.body.forEach(x=> delete x._id);

                    const createdGetCopy = [...createdGetOrders];
                    createdGetCopy.forEach(x=> delete x._id);

                    res.body.should.deep.equal(createdGetCopy.splice(0,5));
                    done();
                });
        });

        it('Should get second page first 5 listings',(done)=>{
            chai.request(server)
                .get('/orders?items=5&page=2')
                .set({authorization: "Basic "+token})
                .send(order)
                .end((err, res) => {
                    res.body.should.have.length(3);
                    res.body.forEach(x=> delete x._id);

                    const createdGetCopy = [...createdGetOrders];
                    createdGetCopy.forEach(x=> delete x._id);

                    res.body.should.deep.equal(createdGetCopy.splice(5,5));
                    done();
                });
        });

        it('Should get 5 orders in reverse',(done)=>{
            chai.request(server)
                .get('/orders?items=5&createdAt=1')
                .set({authorization: "Basic "+token})
                .send(order)
                .end((err, res) => {
                    res.body.should.have.length(5);
                    res.body.forEach(x=> delete x._id);

                    const createdGetCopy = [...createdGetOrders].reverse();
                    createdGetCopy.forEach(x=> delete x._id);

                    res.body.should.deep.equal(createdGetCopy.splice(0,5));
                    done();
                });
        });

        it('Should get 5 orders page 2 in reverse',(done)=>{
            chai.request(server)
                .get('/orders?items=5&createdAt=1&page=2')
                .set({authorization: "Basic "+token})
                .send(order)
                .end((err, res) => {
                    res.body.should.have.length(3);
                    res.body.forEach(x=> delete x._id);

                    const createdGetCopy = [...createdGetOrders].reverse();
                    createdGetCopy.forEach(x=> delete x._id);

                    res.body.should.deep.equal(createdGetCopy.splice(5,5));
                    done();
                });
        });

        after(()=>{
            return db.Orders.collection.remove({});
        })

    });

    describe('PATCH /listings',()=>{
        beforeEach(()=>{
            return db.Orders.insertOne(createdOrder)
        });

        it('Should update all order fields', (done)=>{
            chai.request(server)
                .patch('/orders/'+createdOrder._id.toString())
                .set({authorization: "Basic "+token})
                .send({
                    isPrivate: false,
                    delivery: wiri,
                    pickup: christChurch,
                    restaurant: "Fine French Food",
                    createdBy: "shouldn't@change.com",
                    assignedTo: 456,
                    createdAt: "no change date",
                    distance: "shouldn't change distance",
                    duration: "shouldn't change duration"
                })
                .end((err, res) => {
                    const body = res.body;

                    body.distance.should.not.equal(createdOrder.duration);
                    body.distance.should.not.equal("shouldn't change distance");

                    body.should.not.equal(createdOrder.duration);
                    body.duration.should.not.equal("shouldn't change duration");

                    body._id.toString().should.be.equal(createdOrder._id.toString());
                    body.createdAt.should.be.equal(createdOrder.createdAt);
                    body.isPrivate.should.be.false;
                    body.assignedTo.should.be.equal(456);

                    body.delivery.should.equal(wiri);
                    body.pickup.should.equal(christChurch);

                    body.createdBy.should.equal(user.email);

                    done()
                });
        })

        it('Should update all order fields except delivery, pickup thus not changing distance && duration', (done)=>{
            chai.request(server)
                .patch('/orders/'+createdOrder._id.toString())
                .set({authorization: "Basic "+token})
                .send({
                    isPrivate: false,
                    restaurant: "Fine French Food",
                    createdBy: "shouldn't@change.com",
                    assignedTo: 456,
                    createdAt: "no change date",
                    distance: "shouldn't be change distance",
                    duration: "shouldn't be change duration"
                })
                .end((err, res) => {
                    const body = res.body;

                    body.distance.should.equal(createdOrder.distance);
                    body.distance.should.not.equal("shouldn't be change distance");

                    body.duration.should.equal(createdOrder.duration);
                    body.duration.should.not.equal("shouldn't be change duration");

                    body._id.toString().should.be.equal(createdOrder._id.toString());
                    body.createdAt.should.be.equal(createdOrder.createdAt);
                    body.isPrivate.should.be.false;
                    body.assignedTo.should.be.equal(456);

                    body.delivery.should.equal(takapuna);
                    body.deliveryAddress.should.equal(createdOrder.deliveryAddress);

                    body.pickup.should.equal(palmerstonNorth);
                    body.pickupAddress.should.equal(createdOrder.pickupAddress);

                    body.createdBy.should.equal(user.email);

                    done()
                });
        });

        afterEach(()=>{
            return db.Orders.collection.remove({})
        })
    })

});