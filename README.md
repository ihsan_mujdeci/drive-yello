# Drive Yellow

**Drive Yello - Software Engineer Technical Test**

* Express and mongo app.
* Mocha test runner.
* POST, GET, PATCH Orders.
* Calculate distance from delivery and pickup.
* Basic JWT auth for most endpoints obtained form POST /login.

**Npm scripts**

`npm test` must be used if running tests. This command includes all test config from /config/test-config.js

* Start: `npm start`
* Test: `npm test`

# Schema
## Order
```
{
    delivery: "lat,long",
    deliveryAddress: String
    pickup: "lat,long",
    pickupAddress: String,
    assignedTo: String
    restautrant: String,
    private: Boolean,
    createdAt: Date,
    distance: String,
    duration: String
}
```

## Example response Order

```
{
    "_id": "5a114a05cff35819a8b9ffad",
    "pickup": "-40.354859,175.610485",
    "delivery": "-37.003122,174.864392",
    "assignedTo": "Ihsan",
    "isPrivate": false,
    "restaurant": "Mom & pops",
    "createdBy": "ihsanmujdeci.im@gmail.com",
    "distance": "496 km",
    "duration": "6 hours 8 mins",
    "pickupAddress": "173-175 The Square, Palmerston North, 4410, New Zealand",
    "deliveryAddress": "296-298 Wiri Station Rd, Wiri, Auckland 2104, New Zealand",
    "createdAt": "2017-11-19T09:08:21.964Z"
}
```

# Authorization

Basic auth is used and placed in header. `JWT-TOKEN` is retrieved by **`POST`** `/login` endpoint.
The `JWT-TOKEN` is placed in the `authorization` header. The value is preceded by `Basic` string followed by a space then the `JWT-TOKEN`.

**eg** `curl -H "Authorization: Basic {JWT-TOKEN}"`

Every `orders` endpoint except the **`GET`** `orders` & **`GET`**`orders/:id` endpoints need authorization. Only if the listing is public can a listing be viewed by a non `authorization` request.

# API

* `pickup` and `delivery` to be a array string of lat long like "{lat},{long}", eg `{ pickup: "-40.354859,175.610485"}`

## Put Orders
`POST /orders`

**`headers:`** `{authorization: "Basic {JWT-TOKEN}"}`



**Request**

```
{
    pickup: "lat,long", (required)
    delivery: "lat,long", (required)
    assignedTo: Any,
    isPrivate: Boolean,
    restaurant:String ,
    deliveryAt: Date
}
```

**Response**

```
{
    delivery: String,
    deliveryAddress: String,
    pickup: String,
    pickupAddress: String,
    assignedTo: ID
    restautrant: String,
    isPrivate: Boolean,
    createdBy: String,
    distance: String,
    duration: String,
    createdAt: Date,
}
```

## Get Order
`GET /orders/:id`

**`headers:`** optional `{authorization: "Basic {JWT-TOKEN}"}`

* If headers arent included then only `isPrivate: false` listings will appear

**Reponse**

```
{
    delivery: String,
    deliveryAddress: String,
    pickup: String,
    pickupAddress: String,
    assignedTo: ID
    restautrant: String,
    isPrivate: Boolean,
    createdBy: String,
    distance: String,
    duration: String,
    createdAt: Date,
}
```

## Get Orders
`GET /orders`

**`headers:`** optional `{authorization: "Basic {JWT-TOKEN}"}`

**e.g:** `GET /orders/?createdAt=-1&items=10&page=2`
* If headers arent included then only `isPrivate: false` listings will appear

* Default behviour displays newest to oldest and shows all results possible

**Query Parameters:**
1 : accending
-1: decending

| name       | Description | Type|
| --------   | --------    | --------      |
| createdAt  | Sory by time created At, default decending (newest first).  1 = accending, 1 = decending    | `1, -1` |
| items      | How many orders per page, default 50    | `Number`            |
| page       | Which page of orders, default 1        | `Number`            |

**Response**
```
[
...{
    delivery: String,
    deliveryAddress: String,
    pickup: String,
    pickupAddress: String,
    assignedTo: ID
    restautrant: String,
    isPrivate: Boolean,
    createdBy: String,
    distance: String,
    duration: String,
    createdAt: Date,
   }
]
```

## Patch Order
`PATCH /orders/:id`

**`headers:`** optional `{authorization: "Basic {JWT-TOKEN}"}`


**Reqest**

```
{
    pickup: "lat,long"
    delivery: "lat,long",
    assignedTo: Any,
    isPrivate: Boolean,
    restaurant:String ,
    deliveryAt: Date
}
```

**Response**

```
{
    delivery: String,
    deliveryAddress: String,
    pickup: String,
    pickupAddress: String,
    assignedTo: ID
    restautrant: String,
    isPrivate: Boolean,
    createdBy: String,
    distance: String,
    duration: String,
    createdAt: Date,
}
```

## Auth

`POST  /login`

 A valid email and any password will work. Returns JWT-TOKEN https://jwt.io/. Needed for Basic auth.

**Request**
```
{
    email: String, (required)
    password: String (required)
}
```

**Response**
```
{
    eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkFsaXJlemEgTm9yb3V6aSIsIkhleSI6IlN1cCBtYW4iLCJhZG1pbiI6dHJ1ZX0.sBcs-ovxAv3N6hcf1HP8uhZ0TEmI0XGs85oqrztTVYc
}
```