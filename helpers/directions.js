const {httpsGet} = require('./http');

async function googleDirections(origin, destination){

    const results = await httpsGet(
        "maps.googleapis.com",
        `/maps/api/directions/json?origin=${origin}&destination=${destination}&key=${process.env.googleApi}`
    )

    if(results.status === 'ZERO_RESULTS' || results.status === 'NOT_FOUND'){
        throw {message: "No results were found when calculating directions"}
    }

    return {
        distance: results.routes[0].legs[0].distance.text,
        duration: results.routes[0].legs[0].duration.text,
        pickupAddress: results.routes[0].legs[0].start_address,
        deliveryAddress: results.routes[0].legs[0].end_address
    };
}

module.exports={
    googleDirections
};

//test data
const takapuna = "-36.787304,174.770237";
const palmerstonNorth = "-40.354859,175.610485";

