const jwt = require('./jwt');

function isEmail(field){
    return (req,res,next)=>{

        const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if(!emailRegex.test(req.body.email))
            req.checkBody.push({param: field, message: `${field} is not valid email format`})

        next()
    }
}

function exists(...fields){
    return (req,res,next)=>{

        fields.forEach(field=>{
            if(!req.body[field])
                req.checkBody.push({param: field, message: `${field} is not included in request`})
        });

        next();
    }
}

function checkBody(req){
    if(req.checkBody.length)
        throw req.checkBody
}

async function isAuth(req,res,next){
    try{
        if(req.headers.authorization){
            const authorization = req.headers.authorization.split('Basic')[1].trim();

            req.user = await jwt.verify(authorization);
            next();
        }
        else{
            throw {message: "Authorization not included in header", code: 401}
        }
    }
    catch(err){
        res.status(err.code || 400).send(err)
    }
}

async function guest(req,res,next){
    try{
        if(req.headers.authorization){
            const authorization = req.headers.authorization.split('Basic')[1].trim();
            req.user = await jwt.verify(authorization);
        }
        else{
            req.user = {role: "guest"};
        }
        next()
    }
    catch(err){
        req.user = {role: "guest"};
        next()
    }
}

function filter(...valid){
    return (req,res,next)=>{
        for(let o in req.body){
            if(!valid.includes(o)){
                delete req.body[o]
            }
        }
        next()
    }
}

function isString(...str){
    return (req,res,next)=>{
        str.forEach(s=>{
            if(typeof req.body[s] !== 'string')
                req.checkBody.push({param: s, message: `${s} is not a String`})
        });

        next()
    };
}

function isBoolean(...bool){
    return (req,res,next)=>{
        bool.forEach(b=>{
            if(typeof req.body[b] !== 'boolean')
                req.checkBody.push({param: b, message: `${b} is not a boolean`})
        })

        next()
    };
}

function isNumber(n){
    return typeof n === "number" && isFinite(n)
};

function isLatLong(...latLong){

    return (req,res,next)=>{
        latLong.forEach(l=>{

            if(!req.body[l]){
                req.checkBody.push({param: l,message: "Please include "+l})
                return next()
            }
            const split = req.body[l].split(',').map(x=>parseFloat(x));

            if(split.length !==2){
                req.checkBody.push({param: l,message: "Only 2 items in lat long"})
            }

            for(let s in split){
                if(!isNumber(split[s])){
                    req.checkBody.push({param: l,message: "Lat long must be Int or float"})
                    break;
                }
            }
        });

        next()
    }
}

function boolParser(bool){
    if(bool === "false")
        return false;
    else
        return !!bool
}

function parseBool(...bools){
    return(req,res,next)=>{
        bools.forEach(bool=>{
            if(req.body.hasOwnProperty(bool)){
                req.body[bool] = boolParser(req.body[bool]);
            }
            next()
        })
    }
}

module.exports = {
    isEmail,
    exists,
    checkBody,
    isAuth,
    guest,
    filter,
    isLatLong,
    parseBool
}