const jwt = require('jsonwebtoken');

const cert = "shhhhh";

function sign(data){

    return new Promise((resolve, reject)=>{
        jwt.sign(data, cert, {expiresIn: '1h'},function(err, token) {
            if(err)
                reject(err);
            else
                resolve(token)
        });
    });
}

function verify(token){
    return new Promise((resolve, reject)=>{
        jwt.verify(token, cert, function(err, decoded) {
            if(err)
                reject({message: "Authorization format not valid", code: 401});
            else
                resolve(decoded)
        });
    });
}

module.exports = {
    sign,
    verify
}