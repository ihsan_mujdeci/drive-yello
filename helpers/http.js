const https = require("https")

//might be a bit overkill but took you up on "try to use as much native code as possible"
function httpsGet(host, path) {

    const options = {
        hostname: host,
        port: 443,
        path:   path  ,
        method: 'GET'
    };

    return new Promise((resolve, reject)=>{
        const req = https.request(options, (res) => {

            res.setEncoding("utf8");
            let body = "";

            res.on('data', (d) => {
                body+=d
            });

            res.on('end',()=>{
                const results = JSON.parse(body);

                if(res.statusCode.toString().charAt(0) === "2"){
                    resolve(results)
                }
                else{
                    reject(results)
                }
            })
        });

        req.on('error', (e) => {
            reject(e)
        });

        req.end();
    });

}

module.exports = {
    httpsGet
}